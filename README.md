#Monator Liferay Theme Tool
Monator Liferay Theme Tool (MLTT) is an all-in-one tool to automate Liferay theme development.

The primary function is to give you a better workflow for theme development. The usual workflow for developing themes is:

1. Writing the css/js/ftl/vm
2. Building the theme with tools like ant or mavne.
3. Deploying the theme
4. Visiting the page to see if you did good or bad.

A much faster way of developing themes is to write all your css/js/ftl/vm straight on the server (_tomcat/webapps/my-awesome-theme_). That way you nearly never have to rebuild your theme. All the changes you make are instantly there.

However, you want your code in your source code repository and therefor you need to copy all custom theme files from the server to the repository folder. The server theme folder (_tomcat/webapps/my-awesome-theme_) is a combination of your own files and the files which are inherited - you only want _your_ files. This App takes care of that.

Then there are some helper functions aswell:

    up          - Start everything you've set to start (server/IDE/tail log/grunt watcher)
    down        - Stop server
    build       - Copy files from server to code and build the theme
    clean       - Clean the /temp and /work directories on the server

    init        - Create a config file for a project
    init-theme  - Create a theme config file for a project
    test        - Check the config files (and check that paths exists)

##Dependencies
* Needs [Node.js](http://nodejs.org/) installed.
* Untested on anything else than a Mac. Should work on Windows with some limitations (you can't run the _up_ command as that is build on AppleScript)
    
##Installation
1. Make sure you have Node.js installed. Run `node -v` to get the installed version if you're unsure. If not, [download](http://nodejs.org/) and install Node.js.
2. Place yourself in the folder where you downloaded this app. (You should have the _package.json_ file in this folder).
3. Install all dependencies by running `npm install`.
4. Run `node mltt.js`. That should give you some friendly output helping you set up your first project.

##Initialize a project
It's just to run `node mltt.js` and follow the instructions, but here they are as well.

**1)** Run `node mltt.js your-project-name init`. You'll be writing the project name many times so you probably want to make it short and use _maw_ rather than _My-Awesome-Project_ as a project name. This will create the new config file.

**2)** Edit the config file (_./configs/your-project-name.js_) in your favourite editor.

If you have the same folder structure for all your servers/source code repositories, there's probably just these 3 variables you need to change when creating additional projects:

	config.helper.projectName 
	config.helper.themeName
	config.helper.serverFolderName
	
**3)** Run `node mltt.js your-project-name test` to check all paths and variables in your config file. Make sure it all looks okay. Don't bother about it saying that it can't find the Theme Config File. You'll create it in a second.

**4)** Run `node mltt.js your-project-name init-theme`. This will create a config file (_config.movescript.js_) in your theme root directory (_config.path.source.themeRoot_)

**5)** Edit the theme config file (_your-theme-root/config.movescript.js_) in your favourite editor. This file contains an array of rules of which files are to be copied from the server to the source when running _build_. Where the project config file (created in step 1) has paths which is unique to your machine, this theme config fil is the same for everyone working on the theme. Therefor it's a separate config file which can be checked in to your source code repository.

You can copy

**Individual files**

	config.copyList = [
		'css/aui.css',
		'css/custom.css'
		];

**Folders (and its subfolders recursively)**

	config.copyList = [
		'css/partials',
		'templates',
		'js',
		'images/theme'
		];


**Files matching a [Glob](http://goo.gl/G6QlXs) pattern.**

	config.copyList = [
		'css/_*'			//Copy all files with a name starting with underscore
		];


**And combine it of course. Just like the default theme config:**

	config.copyList = [
		'css/aui.css',
		'css/custom.css',
		'css/_*',
		'css/partials',
		'templates',
		'js',
		'images/theme'
		];

**6)** Run `node mltt.js your-project-name test` again to check all paths. As you've created the theme config it'll check those filepaths as well. If everything is nice and green you're all set. If not - fix it and run test again.




##Functions
####Build
Copy all files from server to source and rebuild the theme.

This App will, when runned with `build`:

1. Copy all custom theme files from _server_ to _source_
2. Build the theme
3. Deploy the theme
 
####Up
Start everything you need to start developing.

This App will, when runned with `up`:

1. Open the project in your IDE
2. Open a new terminal tab and start the Grunt job which watches the scss-files on the server and touch the custom.css if any of the scss partials are updated. (See the _css-toucher_ project)
3. Open a new terminal tab and tail -f catalina.out
4. Open a new terminal tab and start the server (_./startup.sh_)

####Down
Stop the server

This App will, when runned with `down`:

1. Shut down the server (_./shutdown.sh_)

####Clean
Clean temporary folders on the server

This App will, when runned with `clean`:

1. Empty the _temp_ folder on the server (_tomcat/temp_)
2. Empty the _work_ folder on the server (_tomcat/work_)


####Helper functions
#####Init
Create a config file for a project
#####init-theme
Create a theme config file for a project
#####test
Check the config files (and check that paths exists)
